# GitLab testing VM
# ------------------------------------------------------------------------------
# Usage:
#
#    See README.md for details
#
# ------------------------------------------------------------------------------
# -*- mode: ruby
# vi: set ft=ruby :

Vagrant.configure('2') do |config|
  # For a complete reference of configuration options, please see the online
  # documentation at https://docs.vagrantup.com.
  config.vm.box = 'centos/7'
  config.vm.define ENV['VAGRANT_BOX_NAME'] || 'gitlab'

  config.vm.provider 'virtualbox' do |vb|
    vb.customize ['modifyvm', :id, '--ioapic', 'on']
    vb.memory = ENV['VAGRANT_VM_RAM'] || '8192'
    vb.cpus   = ENV['VAGRANT_VM_CPUS'] || '4'
    config.vm.network "forwarded_port", guest: 80, host: ENV.fetch('VAGRANT_VM_GITLAB_HTTP',8080), host_ip: "127.0.0.1"
    config.vm.network "forwarded_port", guest: 443, host: ENV.fetch('VAGRANT_VM_GITLAB_HTTPS',8443), host_ip: "127.0.0.1"
    config.vm.network "forwarded_port", guest: 2022, host: ENV.fetch('VAGRANT_VM_GITLAB_SSH',2202), host_ip: "127.0.0.1"
  end

  config.vm.provision 'shell', inline: 'bash /vagrant/scripts/provision.sh'

  # pass on certain environment variables from the `vagrant CMD` cli to the
  # tasks running in the VM
  bash_env_string = (
    ENV
     .to_h
     .select{ |k,v| k =~ /^SCRIPTS_|GITLAB_|CI_|^DEBUG|^VERBOSE/ }
     .map{|k,v| "#{k}=#{v}"}.join(' ')
  )

  config.vm.provision 'shell', privileged: false, inline: <<-SHELL
    cd /vagrant
    #{bash_env_string} bash scripts/run_tasks.sh
    cd /vagrant
  SHELL
end
