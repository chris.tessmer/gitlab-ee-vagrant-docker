Scaffold GitLab EE in a VM and populate it with users, groups, and projects

<!-- vim-markdown-toc GFM -->

* [Description](#description)
* [Setup](#setup)
* [Usage](#usage)
  * [Env vars](#env-vars)
  * [scripts](#scripts)

<!-- vim-markdown-toc -->

## Description

This Vagrantfile:

* Provisions an EL7 VM
* Installs docker
* Installs a GitLab EE container
* Installs RVM (ruby) and the `gitlab` gem
* Adds Gitlab users, groups, projects (as defined in `data/*.yaml`)

It's a Vagrantfile so you can use it on Linux, Windows, OSX.

## Setup

You need Vagrant and VirtualBox

## Usage

1. Edit the `data/*.yaml` files to taste
2. `vagrant up`  (or with env vars: `VAGRANT_BOX_NAME=gutlob vagrant up`)
3. Connect to Gitlab on local forwarded ports (see table below)
4. (optional) `vagrant ssh` into the VM

### Env vars

|  variable                  | default | purpose                               |
| -------------------------- | ------- | ------------------------------------- |
| `VAGRANT_BOX_NAME`         | `gitlab`  | The name of the VM                    |
| `VAGRANT_BOX_CPUS`         | 4       | VM CPUs                               |
| `VAGRANT_BOX_RAM`          | 8192    | VM RAM                                |
| `VAGRANT_VM_GITLAB_HTTP`   | 8080    | Local port forward for GitLab server HTTP  |
| `VAGRANT_VM_GITLAB_HTTPS`  | 8443    | Local port forward for GitLab server HTTPS |
| `VAGRANT_VM_GITLAB_SSH`    | 2202    | Local port forward for GitLab server HTTPS |
| `SCRIPTS_scaffold_gitlab_tries` | 100 | Max tries to first connect to GitLab |
| `SCRIPTS_scaffold_gitlab_delay` | 30 | Delay in seconds between first connecting to GitLab |
| `GITLAB_ROOT_PASSWORD`     | `password` | Initial Gitlab root password         |


### scripts

* `scripts/provision` sets up the VM
* `scripts/run_tasks.sh` sets up everything else (by running other scripts)
* `scripts/scaffold_gitlab.rb` does what it says




