gitlab_server  = ENV['GITLAB_SERVER_URL']    || 'http://localhost'
gitlab_root_pw = ENV['GITLAB_ROOT_PASSWORD'] || 'password'
access_token   = get_root_access_token(gitlab_server, gitlab_root_pw)

Gitlab.endpoint = "#{gitlab_server}/api/v4"
Gitlab.private_token = access_token

script_path   = File.dirname(__FILE__)

if ARGV.size < 3
  STDERR.puts "Usage:\n\t#{File.dirname(__FILE__)} user/project runner"
end