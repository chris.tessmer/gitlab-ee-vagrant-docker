#!/bin/bash

cd /vagrant

# grab containers
docker pull gitlab/gitlab-ee
docker pull gitlab/gitlab-runner

umask 0022

sudo bash scripts/05_install_dnsmasq.sh

# start gitlab
sudo docker run --detach \
    --hostname gitlab.vagrant.wat \
    --env GITLAB_OMNIBUS_CONFIG="${GITLAB_OMNIBUS_CONFIG:-external_url 'http://gitlab.vagrant.wat/'; gitlab_rails['lfs_enabled'] = true;}" \
    --env GITLAB_ROOT_PASSWORD="${GITLAB_ROOT_PASSWORD:-password}" \
    --env "GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN=GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN" \
    --publish 443:443 --publish 80:80 --publish 2202:22 \
    --name gitlab \
    --restart always \
    --volume /srv/gitlab/config:/etc/gitlab:Z \
    --volume /srv/gitlab/logs:/var/log/gitlab:Z \
    --volume /srv/gitlab/data:/var/opt/gitlab:Z \
    gitlab/gitlab-ee:latest


# install ruby
source scripts/10_install_rvm.sh

# scaffold gitlab (users, groups, projects)
gem install gitlab
ruby scripts/scaffold_gitlab.rb

# TODO: install gitlab-runner
# curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash

