require 'net/http'
require 'uri'
require 'json'
require 'yaml'
require 'gitlab'

# From https://docs.gitlab.com/ee/api/oauth2.html#resource-owner-password-credentials
#
# We can do this because
# (1) We have not turned on two-factor authentication for the GitLab root user
# (2) We are in a totally trusted environment.
#
# WARNING: DON'T EVER DO THIS ON A REAL SYSTEM.  This is NOT secure.
def get_root_access_token(gitlab_server, gitlab_root_pw)
  json = "{\"grant_type\": \"password\", \"username\": \"root\", \"password\": \"#{gitlab_root_pw}\"}"
  uri = URI.parse("#{gitlab_server}/oauth/token")

  # keep trying in case gitlab isn't ready yet
  max_tries        = ENV['SCRIPTS_scaffold_gitlab_tries'] || 100
  delay_in_seconds = ENV['SCRIPTS_scaffold_gitlab_delay'] || 30
  tries    = 0
  response = ''
  loop do
    tries += 1
    puts "=== attempting to get GitLab token (pass #{tries})"

    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/json"
    request.body         = json
    req_options          = { use_ssl: uri.scheme == "https" }
    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end

    break if response.code_type == Net::HTTPOK
    puts "got response: #{response.code_type} ( #{response.code})"
    if tries >= max_tries
      fail "Tried #{tries} times to get a GitLab token; giving up!"
    end
    puts "...waiting for #{delay_in_seconds} seconds before trying again"
    sleep delay_in_seconds
  end

  JSON.parse(response.body)['access_token']
end



def create_users(users_data)
  users = {}
  puts "=== scaffolding GitLab users"
  users_data.each do |_id, data|
    opts = {reset_password: false, skip_confirmation: true}.merge(data[:opts])
    begin
      users[_id] = Gitlab.create_user(data[:email], data[:password], _id, opts)
      puts "Created Gitlab user #{_id}"
    rescue Gitlab::Error::Conflict, Gitlab::Error::BadRequest => e
      warn "WARNING: Problem creating user #{_id}: #{e} (#{e.class})"
    end
  end
  users
end

def create_groups(groups_data, users)
  puts "=== scaffolding GitLab groups"
  access_levels = {guest: 10, reporter: 20, developer: 30, maintainer: 40}
  groups = {}
  groups_data.each do |g, data|
    begin
      groups[g.to_sym] = Gitlab.create_group g, g
      puts "== Created Gitlab group #{g}"
    rescue Gitlab::Error::Conflict, Gitlab::Error::BadRequest => e
      warn "WARNING: Problem creating group #{g}:\n\t#{e}\n\t(#{e.class})"
    end

    begin
      group = Gitlab.group_search(g).first
      data[:access_levels].each do |level, members|
        members.each do |_user|
          user = Gitlab.user_search(_user).first
          Gitlab.add_group_member(group.id, user.id, access_levels[level])
          puts "  Added user '#{_user.to_s}' as '#{level.to_s}'"
        end
      end
    rescue Gitlab::Error::Conflict, Gitlab::Error::BadRequest => e
      warn "WARNING: Problem creating group #{g}:\n\t#{e}\n\t(#{e.class})"
    end
  end
  groups
end


def create_projects(projects_data, groups)
  puts "=== scaffolding GitLab projects"
  projects = {}
  projects_data.each do |_id, data|
    data[:namespace_id] = groups[data[:namespace]].to_h['id']
    data.delete :namespace
    begin
      projects[_id] = Gitlab.create_project( _id.to_s, data )
      puts "Created Gitlab project #{_id}"
    rescue Gitlab::Error::Conflict, Gitlab::Error::BadRequest => e
      warn "WARNING: Problem creating project #{_id}:\n\t#{e}\n\t(#{e.class})"
    end
  end
end




gitlab_server  = ENV['GITLAB_SERVER_URL']    || 'http://localhost'
gitlab_root_pw = ENV['GITLAB_ROOT_PASSWORD'] || 'password'

Gitlab.endpoint      = "#{gitlab_server}/api/v4"
Gitlab.private_token = get_root_access_token(gitlab_server, gitlab_root_pw)

data_path     = File.expand_path('../data',File.dirname(__FILE__))
users_data    = YAML.load_file File.join(data_path,'users_data.yaml')
groups_data   = YAML.load_file File.join(data_path,'groups_data.yaml')
projects_data = YAML.load_file File.join(data_path,'projects_data.yaml')

users    = create_users(users_data)
groups   = create_groups(groups_data, users)
projects = create_projects(projects_data, groups)
