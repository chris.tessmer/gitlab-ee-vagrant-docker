#!/bin/bash
# set up dnsmasq to provide a local DNS domain
# (run as root)

yum install -y dnsmasq
cat << DNSMASQ_NM > /etc/NetworkManager/conf.d/50-dnsmasq.conf
[main]
dns=dnsmasq
DNSMASQ_NM

mkdir -p /etc/NetworkManager/dnsmasq.d/
cat << DNSMASQ > /etc/NetworkManager/dnsmasq.d/dnsmasq.conf
local=/vagrant.wat/
domain=vagrant.wat
conf-dir=/etc/dnsmasq.d,.rpmnew,.rpmsave,.rpmorig
DNSMASQ

systemctl restart NetworkManager

